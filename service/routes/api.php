<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE');
header('Access-Control-Allow-Headers: Authorization, Content-Type');

Route::group(['prefix' => 'hero'], function ()
{
	Route::get('index',			'HeroController@index');
	Route::get('show',			'HeroController@show');
	Route::post('store',		'HeroController@store');
	Route::delete('destroy',	'HeroController@destroy');
});

Route::group(['prefix' => 'team'], function ()
{
	Route::get('index',			'TeamController@index');
	Route::get('show',			'TeamController@show');
	Route::post('store',		'TeamController@store');
	Route::delete('destroy',	'TeamController@destroy');
});