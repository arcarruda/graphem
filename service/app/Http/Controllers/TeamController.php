<?php

namespace App\Http\Controllers;

use App\Team;
use App\Hero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::orderBy('id', 'DESC')->with(['heroes'])->paginate(50);
        foreach ($teams as $team) {
            $team->combat_power = 0;
            foreach ($team->heroes as $hero) {
                $team->combat_power += $hero->points;
            }
        }
        return response()->json(compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $rules = [
            'name'          => 'required|string|max:100',
            'heroes'        => 'required|array|min:3|max:5',
            'heroes.*.id'   => 'required|integer|exists:heroes,id,team_id,NULL,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $team = Team::create($request->all());
        $heroes = Hero::whereIn('id', array_column($request->heroes, 'id'))
            ->update(['team_id' => $team->id]);
        return response()->json([
            'message' => 'Team created.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:teams,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $team = Team::with('heroes')->find($request->id);
        return response()->json(compact('team'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:teams,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        if(Team::destroy($request->id)) {
            return response()->json([
                'message' => 'Team removed.'
            ]);
        }
        return response()->json([
            'message' => 'Some error occurred. Contact us.'
        ], 400);
    }
}
