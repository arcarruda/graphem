<?php

namespace App\Http\Controllers;

use App\Hero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HeroController extends Controller
{
    /**
     * Display a listing of the Hero.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $heroes = Hero::orderBy('id', 'DESC');
        if($request->has('side')) {
            $heroes->whereSide($request->side);
        }
        $heroes = $heroes->paginate(50);
        return response()->json(compact('heroes'));
    }

    /**
     * Store a newly created Hero in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:100',
            'side' => 'required|string|in:light,dark',
            'points' => 'required|numeric',
            'attack' => 'required|boolean',
            'special_ability' => 'required|string|max:50'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $hero = Hero::create($request->all());
        return response()->json([
            'message' => 'Hero created.'
        ]);
    }

    /**
     * Display the specified Hero.
     *
     * @param  \App\Hero  $hero
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:heroes,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $hero = Hero::with('team')->find($request->id);
        return response()->json(compact('hero'));
    }

    /**
     * Remove the specified Hero from storage.
     *
     * @param  \App\Hero  $hero
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {   
        $rules = [
            'id' => 'required|integer|exists:heroes,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        if(Hero::destroy($request->id)) {
            return response()->json([
                'message' => 'Hero removed.'
            ]);
        }
        return response()->json([
            'message' => 'Some error occurred. Contact us.'
        ], 400);
    }
}
