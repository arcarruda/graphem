<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hero extends Model
{
    use SoftDeletes;
    
	protected $fillable = [
		'name',
        'side',
        'points',
        'attack',
        'special_ability'
	];
    public function team () 
    {
    	return $this->belongsTo('App\Team');
    }
}
