<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heroes', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('team_id')->nullable()->index('fk_heroe_team_idx');
            $table->string('name', 100);
            $table->enum('side', ['light', 'dark']);
            $table->decimal('points', 8, 2);
            $table->boolean('attack');
            $table->string('special_ability', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heroes');
    }
}
