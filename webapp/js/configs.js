"use strict";
application
	.config(function(
		$httpProvider, 
		$locationProvider, 
		$mdThemingProvider, 
		$mdGestureProvider, 
		$mdDateLocaleProvider
	) {
		$locationProvider.html5Mode(true);
		$httpProvider.interceptors.push('httpInterceptor');
		$mdGestureProvider.skipClickHijack();
		var themingProvider = $mdThemingProvider.theme('default')
    		.primaryPalette('blue')
    		.accentPalette('indigo')
    		.warnPalette('red');
	})
	.value('config', {
	    service_url : "http://service.graphem.vnvr.solutions",
	});
