"use strict";
application
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) 
    {
        $urlRouterProvider.otherwise('/myheroes');
        $stateProvider
            .state('404', {
                views : {
                    'content@' : {
                        templateUrl     : 'views/404.html?v=' + Math.random(),
                    }
                },
                url     : '/404',
            })
            .state('heroIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/hero/index.html?v=' + Math.random(),
                        controller      : 'HeroIndexController',
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/myheroes',
            })
            .state('heroStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/hero/store.html?v=' + Math.random(),
                        controller      : 'HeroStoreController',
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/myheroes/createhero',
            })
            .state('teamIndex', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/team/index.html?v=' + Math.random(),
                        controller      : 'TeamIndexController',
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/myheroes/teams',
            })
            .state('teamStore', {
                views : {
                    'navigationBar@' : {
                        templateUrl     : 'views/layout/navigationBar.html?v=' + Math.random(),
                    },
                    'content@' : {
                        templateUrl     : 'views/page/team/store.html?v=' + Math.random(),
                        controller      : 'TeamStoreController',
                    },
                    'sideNav@' : {
                        templateUrl     : 'views/layout/sideNav.html?v=' + Math.random() * 10
                    },
                },
                url : '/myheroes/createteam',
            })
        ;
    });