"use strict";
application
	.controller('TeamIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		config) 
	{
		$rootScope.navigationTitle = "My Teams"
		$scope.filter = {}
		var filterSubmited;
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.teams = [];
				if(force) {
					$scope.teams = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {				
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					$http({
						headers : {
							'Accept' : 'application/json',
						},
						method  : 'GET',
						url 	: config.service_url + '/api/team/index',
						params 	: $scope.filter
					}).then(function(response) {
						if(response.status == 200) {
							$scope.teams = $scope.teams.concat(response.data.teams.data);
							$scope.total = response.data.teams.total;
							if(response.data.teams.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		$scope.delete = function (team) {
			confirm('Are you sure you want to delete?', function() {
				$http({
					url 				: config.service_url + '/api/team/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
					},
					params 				: team,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.index(true, true);
						alert(response.data.message, null, 'Success');
					}
				});
			}, null, 'Warn', 'Yes', 'No');
		}
	})
;