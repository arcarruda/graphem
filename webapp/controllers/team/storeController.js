"use strict";
application
	.controller('TeamStoreController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.team = {};
		$scope.filter = {};
		$rootScope.navigationTitle = "Create Team";
		$scope.filter = {}
		$scope.selected = [];
		var filterSubmited;
		$rootScope.backPageTo('teamIndex');
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.heroes = [];
				if(force) {
					$scope.heroes = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {				
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					$http({
						headers : {
							'Accept' : 'application/json',
						},
						method  : 'GET',
						url 	: config.service_url + '/api/hero/index',
						params 	: $scope.filter
					}).then(function(response) {
						if(response.status == 200) {
							$scope.heroes = $scope.heroes.concat(response.data.heroes.data);
							$scope.total = response.data.heroes.total;
							if(response.data.heroes.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		$scope.submit = function () {
			$scope.team.heroes = $scope.selected;
			$http({
				url 				: config.service_url + '/api/team/store',
				method  			: 'POST',
				headers : {
					'Accept' 		: 'application/json',
				},
				data 				: $scope.team,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('teamIndex');
					}, 'Success');
				}
			});
		}
		$scope.isChecked = function() {
			return $scope.selected.length === $scope.items.length;
		};
		$scope.toggle = function (item, list) {
			var idx = list.indexOf(item);
			if (idx > -1) {
				list.splice(idx, 1);
			}
			else {
				list.push(item);
			}
		};

	})
;