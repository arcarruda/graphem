"use strict";
application
	.controller('RootController', function (
		$http,
		$state,
		$window,
		$timeout,
		$mdDialog,
		$mdSidenav,
		$rootScope,
		config) 
	{
		var requestResponseStack = [];
		$rootScope.$on('loading', function(event, flag) {
			if(flag) 	
				angular.element('div[id="loader"]').show();
			else
				angular.element('div[id="loader"]').hide();
			$rootScope.loading = flag;
		});
		$rootScope.toggleMenu = function() {
			$mdSidenav('left').toggle();
		};
		$rootScope.backPageTo = function (to, params)
		{
			$rootScope.backPage = function ()
			{
				angular.element('.content-container > div').removeClass('rtl');
				angular.element('.content-container > div').addClass('ltr');
				$timeout(function() { $state.go(to, params); }, 1);
			};
		};
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			$rootScope.$on('$stateChangeStart', stateChangeStartEvent);
		});
		$rootScope.$on('$stateChangeStart', stateChangeStartEvent);
		function stateChangeStartEvent (event, toState, toParams, fromState, fromParams) {
			delete $rootScope.backPage;
			$rootScope.$offAll('error');
			$rootScope.$on('error', error);
			$rootScope.$offAll('$stateChangeStart');
		}
		$rootScope.$on('request', function() {
			requestResponseStack.push('');
			$rootScope.$broadcast('loading', true);
		});
		$rootScope.$on('response', function() {
			requestResponseStack.pop();
			if(!requestResponseStack || !requestResponseStack.length)
				$rootScope.$broadcast('loading', false);
		});
		
		function error (event, data, status) {
			requestResponseStack = [];
			var message = '';
			if(data) {
				function crackMessages(o, indent) {
					var out = [];
				    if (typeof indent === 'undefined') {
				        indent = 0;
				    }
				    for (var p in o) {
				        if (o.hasOwnProperty(p)) {
				            var val = o[p];
				            if (typeof val === 'object') {
								out.push( crackMessages(val, indent + 1) + new Array(4 * indent + 1).join(' ') );
				            } else {
				                out.push( val );
				            }
				        }
				    }
				    return out.join('<BR>');
				}
				message = crackMessages(data.errors);
			}
			message = message != '' ? message : 'Some error occurred. Contact us.';
			if(!$rootScope.blockShowError) {
				alert(message, function() {
					if(status == 404) {
						$state.go('404');
					}
				}, 'Warn', 'OK');
			} else {
				$rootScope.blockShowError = message;
			}
			$rootScope.$broadcast('loading', false);
		};
		$window.alert = function(message, resultFunction, title, buttonLabel) {
			var dialogShow;
			var alertDialog = $mdDialog.alert();
			alertDialog.parent(angular.element( document.body ));
			alertDialog.clickOutsideToClose(false);
			alertDialog.htmlContent(message);
			if(!title)
				title = 'Message';
			alertDialog.title(title);
			alertDialog.ariaLabel(title);
			if(!buttonLabel)
				buttonLabel = 'OK';
			alertDialog.ok(buttonLabel);
    		alertDialog.multiple(true);
			dialogShow = $mdDialog.show(alertDialog);
			if(resultFunction) {
				dialogShow.then(resultFunction);
			}
		};
		$window.confirm = function(message, successFunction, cancelFunction, title, okLabel, cancelLabel) {
			var dialogShow;
			var confirmDialog = $mdDialog.confirm();
			confirmDialog.parent(angular.element( document.body ));
			confirmDialog.clickOutsideToClose(false);
			confirmDialog.textContent(message);
			if(!title)
				title = 'Message';
			confirmDialog.title(title);
			confirmDialog.ariaLabel(title);
			if(!okLabel)
				okLabel = 'OK';
			confirmDialog.ok(okLabel);
			if(!cancelLabel)
				cancelLabel = 'Cancel';
			confirmDialog.multiple(true);
			confirmDialog.cancel(cancelLabel);
			dialogShow = $mdDialog.show(confirmDialog);
			if(successFunction || cancelFunction) {
				dialogShow.then(successFunction, cancelFunction);
			}
		};
	})
	.run(function($rootScope, $state, $window){
	    $rootScope.$off = function (name, listener) {
	        var namedListeners = this.$$listeners[name];
	        if(namedListeners) {
	            for (var i = 0; i < namedListeners.length; i++) {
	                if(namedListeners[i] === listener) {
	                    return namedListeners.splice(i, 1);
	                }
	            }
	        }
	    }
	    $rootScope.$offAll = function (name) {
	        delete this.$$listeners[name];
	    }
	})
;