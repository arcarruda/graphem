"use strict";
application
	.controller('HeroIndexController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		config) 
	{
		$rootScope.navigationTitle = "My Heroes"
		$scope.filter = {}
		var filterSubmited;
		$scope.index = function (start, force) {
			if(start) {
				$scope.page = 1;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited))
					$scope.heroes = [];
				if(force) {
					$scope.heroes = [];
					filterSubmited = null;
				}
			}
			if($scope.page > 0) {				
				$scope.filter.page = $scope.page;
				if(JSON.stringify($scope.filter) != JSON.stringify(filterSubmited)) {
					filterSubmited = angular.copy($scope.filter);
					$http({
						headers : {
							'Accept' : 'application/json',
						},
						method  : 'GET',
						url 	: config.service_url + '/api/hero/index',
						params 	: $scope.filter
					}).then(function(response) {
						if(response.status == 200) {
							$scope.heroes = $scope.heroes.concat(response.data.heroes.data);
							$scope.total = response.data.heroes.total;
							if(response.data.heroes.next_page_url != null) {
								$scope.page++; 
							} else {
								$scope.page = 0;
							}
						}
					});
				}
			}
		}
		$scope.index(true);
		$scope.delete = function (hero) {
			confirm('Are you sure you want to delete?', function() {
				$http({
					url 				: config.service_url + '/api/hero/destroy',
					method  			: 'DELETE',
					headers : {
						'Accept' 		: 'application/json',
					},
					params 				: hero,
				}).then(function(response) {
					if(response.status == 200) {
						$scope.index(true, true);
						alert(response.data.message, null, 'Success');
					}
				});
			}, null, 'Warn', 'Yes', 'No');
		}
	})
;