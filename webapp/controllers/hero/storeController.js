"use strict";
application
	.controller('HeroStoreController', function (
		$http,
		$state,
		$scope,
		$rootScope,
		$mdDialog,
		config) 
	{
		$scope.hero = {};
		$rootScope.navigationTitle = "Create Hero"
		$rootScope.backPageTo('heroIndex');
		
		$scope.isOpen = false;
        $scope.selectedDirection = 'up';
        $scope.selectedMode = 'md-scale';
		
		$scope.submit = function () {
			$http({
				url 				: config.service_url + '/api/hero/store',
				method  			: 'POST',
				headers : {
					'Accept' 		: 'application/json',
				},
				data 				: $scope.hero,
			}).then(function(response) {
				if(response.status == 200) {
					alert(response.data.message, function() {
						$state.go('heroIndex');
					}, 'Success');
				}
			});
		}
	})
;